package main

import (
	"net"
	"fmt"
	"strings"
	"strconv"
)

type Client struct {
	ID int
	Alive bool
	Connection net.Conn
	ModuleIP string
	ModulePort string
	ModuleName string
	ConnectionCount int64
	KiosksCount int64
	TerminalsCount int64
	UndefinedCount int64
}
// слушаем дискриптор
func (c *Client) ListenDescriptor(conn net.Conn) {
	fmt.Println("Listening connection", conn)
	//слушаем соединение с клиентом
	c.Connection = conn
	c.Alive = true
	for {
		// вообще говоря можно переделать чтение из соединения через io.Copy(&buf, conn)
		// но не вижу в этом смысла (http://stackoverflow.com/questions/24339660/read-whole-data-with-golang-net-conn-read)
		buf := make([]byte, 2048)
		len, err := conn.Read(buf)
		if err != nil {
			fmt.Println("Client connection error")
			break
		}
		c.deserializeData(buf[4:len])// тут удалются первые 4байта потому что это дилина которая поидее вообще не нужна
	}
	c.Alive = false
	fmt.Println("Close connection ", conn)
	conn.Close()
}
// разбираем данные
func (c *Client) deserializeData (data []byte) {
	fmt.Println("Deserialize data from client")
	s := string(data)
	temp := strings.Split(s, "\n")
	c.ModuleIP = temp[0]
	c.ModulePort = temp[1]
	c.ModuleName = temp[2]
	c.ConnectionCount, _ = strconv.ParseInt(temp[3], 10 , 64)
	c.KiosksCount, _ = strconv.ParseInt(temp[4], 10 , 64)
	c.TerminalsCount, _ = strconv.ParseInt(temp[5], 10 , 64)
	c.UndefinedCount, _ = strconv.ParseInt(temp[6], 10 , 64)
}

func (c Client) GetConnection () net.Conn {
	return c.Connection
}

