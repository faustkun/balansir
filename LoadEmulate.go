package main

import (
	"net/http"
	"io/ioutil"
	"time"
	"fmt"
)

func main() {

	for {
		resp, err := http.Get("http://example.com/")
		if err != nil {
			fmt.Println(err)
		}
		defer resp.Body.Close()
		_, err1 := ioutil.ReadAll(resp.Body)
		if err1 != nil {
			fmt.Println(err1)
		}
		time.Sleep(time.Microsecond * 1)
	}

}

