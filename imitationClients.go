package main

import (
	"net"
	"time"
	"sync"
	"strings"
	"strconv"
	"fmt"
)

func imitation(address, data string)  {
	fmt.Println(address, data)
	conn, err := net.Dial("tcp", address)
	if err != nil{
		return
	}
	for {
		b := []byte(data)
		conn.Write(b)
		time.Sleep(time.Second * 10)
	}
}


const COUNT = 10

func main() {
	w := sync.WaitGroup{}
	w.Add(1)
	for i:=0; i < COUNT; i++ {
		go imitation("127.0.0.1:8081", strings.Join(
			[]string{
				string([]byte{1, 2, 3, 4}), // для имитации чисеки длины пакета, котора не нужна
				"127.0.0.", strconv.FormatInt(int64(i), 10),
			"\n100", strconv.FormatInt(int64(i), 10),
			"\ntest server", strconv.FormatInt(int64(i), 10),
			"\n\n",
			strconv.FormatInt(int64(2), 10),
			"\n",
			strconv.FormatInt(int64(3), 10),
			"\n",
			strconv.FormatInt(int64(4), 10)},
			""))
	}

	for i:=0; i < COUNT; i++ {
		go imitation("127.0.0.1:8083", strings.Join(
			[]string{
				string([]byte{1, 2, 3, 4}), // для имитации чисеки длины пакета, котора не нужна
				"127.0.0.", strconv.FormatInt(int64(i), 10),
				"\n200", strconv.FormatInt(int64(i), 10),
				"\ntest server", strconv.FormatInt(int64(i), 10),
				"\n\n",
				strconv.FormatInt(int64(2), 10),
				"\n",
				strconv.FormatInt(int64(3), 10),
				"\n",
				strconv.FormatInt(int64(4), 10)},
			""))
	}
	w.Wait()
}