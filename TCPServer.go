package main

import (
	"os"
	"fmt"
	"net"
	"strings"
	"strconv"
)

const (
	CONN_HOST = "localhost"
	//CONN_PORT = "3333"
	CONN_TYPE = "tcp"
)

type TCPServer struct {
	Port string
	//TODO: подумать нужно ли блокировать клиентов мьютексами
	Clients [](*Client)
}

// слушаем дискриптор
func (c *TCPServer) Listen(port string) {
	// начинаем слушать порт
	c.Port = port
	fmt.Println("TCP-server listen on port ", port)
	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+ port)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// закрываем после окончания работы
	defer l.Close()
	for {
		// ждем подключение
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// отдаем дескриптор клиенту
		newClient := new(Client)
		// добавляем нового клиента в список клиентов
		c.Clients = append(c.Clients, newClient)
		go newClient.ListenDescriptor(conn)
	}
}

// функция выбора лучшего модуля
//TODO: нуждается в полном переписывании
func (c TCPServer) getBestModule() string {
	fmt.Println("Get best module")
	c.DeleteDeadClients()
	if len(c.Clients) > 0 {
		bestClient := c.Clients[0]
		fmt.Println(bestClient)
		for _, client := range c.Clients {
			if client.ConnectionCount < bestClient.ConnectionCount {
				bestClient = client
			}
		}
		return bestClient.ModuleIP + ":" + bestClient.ModulePort
	}
	return ""
}

// возвращает статистику по всем серверам
func (c TCPServer) getStatistics() string {
	fmt.Println("Get statistics")
	//вернуть статистику
	c.DeleteDeadClients()
	temp := []string{}
	temp = append(temp, "Modules count: ")
	temp = append(temp, strconv.FormatInt(int64(len(c.Clients)), 10))
	temp = append(temp, "\r\n")
	var summConnections int64 = 0
	for _, client := range c.Clients {
		temp = append(temp, client.ModuleIP)
		temp = append(temp, ":")
		temp = append(temp, client.ModulePort)
		temp = append(temp, " - clients: ")
		temp = append(temp, strconv.FormatInt(client.ConnectionCount, 10))
		temp = append(temp, "; kiosks: ")
		temp = append(temp, strconv.FormatInt(client.KiosksCount, 10))
		temp = append(temp, "; terminals: ")
		temp = append(temp, strconv.FormatInt(client.TerminalsCount, 10))
		temp = append(temp, "; undefined: ")
		temp = append(temp, strconv.FormatInt(client.UndefinedCount, 10))
		temp = append(temp, "\r\n")
		summConnections += client.ConnectionCount
	}
	temp = append(temp, "\r\n summ of connectins: ")
	temp = append(temp, strconv.FormatInt(summConnections, 10))
	return strings.Join(temp, "")
}

// удаляет померших клиентов из списка клиетов
func (c *TCPServer) DeleteDeadClients() {
	fmt.Println("Deleting dead connections")
	temp := [](*Client){}
	for _, client := range c.Clients {
		if client.Alive == true {
			temp = append(temp, client)
		}
	}
	c.Clients = temp
}




