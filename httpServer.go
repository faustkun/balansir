package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/fasthttp"
	"strings"
	"fmt"
	//"time"
)

type HTTPServer struct {
	srvTcp *TCPServer
	srvWs *TCPServer
}

// запуск сервера
func (c HTTPServer) RunServer(port string) {
	e := echo.New()
	c.srvTcp = new(TCPServer)
	c.srvWs = new(TCPServer)
	go c.srvTcp.Listen("8081")
	go c.srvWs.Listen("8083")

	//должен возвражать ip:port предпочтительного сервера
	e.GET("/getSocket", func(q echo.Context) error {
		response := c.srvTcp.getBestModule()
		return q.String(200, response)
	})

	//аналогично для вебСокета
	e.GET("/getWebSocket", func(q echo.Context) error {
		//response := "HTTP/1.1 200 OK\r\n\r\n" + c.srvWs.getBestModule()
		fmt.Println(c)
		response := c.srvWs.getBestModule()
		return q.String(200, response)
	})

	//получение статов всех серверов (socket/webSocket), для сбора статистики Колей
	e.GET("/getStats", func(q echo.Context) error {
		response := []string{"TCP:\r\n", c.srvTcp.getStatistics(), "\r\n________________\r\nWS:\r\n", c.srvWs.getStatistics()}
		return q.String(200, strings.Join(response, ""))
	})

	e.Run(fasthttp.New(":" + port))
}

